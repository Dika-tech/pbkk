<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <title>Document</title>
</head>
<body>
    <h1>Form Pendaftaran</h1>
    <form action="{{ url('/mahasiswa/update') }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $mahasiswa->id }}">
        <div class="form-group">
            <label>NIM</label>
            <input class="form-control" type="text" name="nim" value="{{ $mahasiswa->nim }}" placeholder="Masukkan NIM Anda">
        </div>
        <div class="form-group">
            <label>Nama</label>
            <input class="form-control" type="text" name="nama" value="{{ $mahasiswa->nama }}" placeholder="Masukkan Nama Lengkap">
        </div>
        <div class="form-group">
            <label>Jenis Kelamin</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jenis_kelamin" value="L" @if($mahasiswa->jenis_kelamin == 'L') checked @endif>
                <label class="form-check-label">Laki-laki</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jenis_kelamin" value="P" @if($mahasiswa->jenis_kelamin == 'P') checked @endif>
                <label class="form-check-label">Perempuan</label>
            </div>
        </div>
        <div class="form-group">
            <label>Jurusan</label>
            <select name="prodi_id" class="form-control">
                @foreach ($prodi as $pr)
                    <option value="{{ $pr->id }}" @if($pr->id == $mahasiswa->prodi_id) selected @endif>{{ $pr->nama }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" class="form-control" cols="30" rows="10">{{ $mahasiswa->alamat }}</textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Simpan</button>
        </div>
    </form>
</body>
</html>
