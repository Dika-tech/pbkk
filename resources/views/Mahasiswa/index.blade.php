<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <title>Document</title>
</head>
<body>
    <h1>Data Pendaftaran Mahasiswa</h1>
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">NIM</th>
                <th scope="col">Nama</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($mahasiswa as $mhs)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $mhs->nim }}</td>
                <td>{{ $mhs->nama }}</td>
                <td>
                    <a href="{{ url('mahasiswa/show/'.$mhs->id) }}">Show</a>
                    @auth
                    <a href="{{ url('mahasiswa/edit/'.$mhs->id) }}">Edit</a>
                    <a href="{{ url('mahasiswa/delete/'.$mhs->id) }}" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')">Delete</a>
                    @endauth
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
