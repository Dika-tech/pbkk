<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <title>Document</title>
</head>
<body>
    <h1>Form Edit Prodi</h1>
    <form action="{{ url('/prodi/update') }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $prodi->id }}">
        <div class="form-group">
            <label>Nama</label>
            <input class="form-control" type="text" name="nama" value="{{ $prodi->nama }}" placeholder="Masukkan Nama Prodi">
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Update</button>
        </div>
    </form>
</body>
</html>
