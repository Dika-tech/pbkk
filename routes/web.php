<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\ProdiController;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('mahasiswa')->group(function () {
    Route::get('/',[MahasiswaController::class,'index']);
    Route::get('/show/{id}',[MahasiswaController::class,'show']);

    Route::middleware(['auth'])->group(function() {
        Route::get('/create',[MahasiswaController::class,'create']);
        Route::post('/store',[MahasiswaController::class,'store']);
        Route::get('/edit/{id}',[MahasiswaController::class,'edit']);
        Route::post('/update',[MahasiswaController::class,'update']);
        Route::get('/delete/{id}',[MahasiswaController::class,'destroy']);
    });
});

Route::prefix('prodi')->group(function () {
    Route::get('/',[ProdiController::class,'index']);
    Route::get('/show/{id}',[ProdiController::class,'show']);

    Route::middleware(['auth'])->group(function() {
        Route::get('/create',[ProdiController::class,'create']);
        Route::post('/store',[ProdiController::class,'store']);
        Route::get('/edit/{id}',[ProdiController::class,'edit']);
        Route::post('/update',[ProdiController::class,'update']);
        Route::get('/delete/{id}',[ProdiController::class,'destroy']);
    });
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
