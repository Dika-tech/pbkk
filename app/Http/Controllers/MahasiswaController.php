<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Models\Prodi;

class MahasiswaController extends Controller
{
    public function index()
    {
        $mahasiswa = Mahasiswa::get();
        return view('Mahasiswa.index',['mahasiswa' => $mahasiswa]);
    }

    public function create()
    {
        $prodi = Prodi::get();
        return view('Mahasiswa.create', ['prodi' => $prodi]);
    }

    public function store(Request $request)
    {
        $mahasiswa = Mahasiswa::create([
            'nim' => $request->nim,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'prodi_id' => $request->prodi_id,
            'alamat' => $request->alamat
        ]);
        return redirect('/mahasiswa');
    }

    public function show($id)
    {
        $mahasiswa = Mahasiswa::with('prodi')->where('id',$id)->first();
        return view('Mahasiswa.show', ['mahasiswa' => $mahasiswa]);
    }

    public function edit($id)
    {
        $prodi = Prodi::get();
        $mahasiswa = Mahasiswa::where('id',$id)->first();
        return view('Mahasiswa.edit', ['mahasiswa' => $mahasiswa, 'prodi' => $prodi]);
    }

    public function update(Request $request)
    {
        $mahasiswa = Mahasiswa::where('id',$request->id)->update([
            'nim' => $request->nim,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'prodi_id' => $request->prodi_id,
            'alamat' => $request->alamat
        ]);
        return redirect('/mahasiswa');
    }

    public function destroy($id)
    {
        $mahasiswa = Mahasiswa::where('id',$id)->delete();
        return redirect('/mahasiswa');
    }
}
