<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Prodi;

class ProdiController extends Controller
{
    public function index()
    {
        $prodi = Prodi::get();
        return view('Prodi.index',['prodi' => $prodi]);
    }

    public function create()
    {
        return view('Prodi.create');
    }

    public function store(Request $request)
    {
        $prodi = Prodi::create([
            'nama' => $request->nama,
        ]);
        return redirect('/prodi');
    }

    public function show($id)
    {
        $prodi = Prodi::where('id',$id)->first();
        return view('Prodi.show', ['prodi' => $prodi]);
    }

    public function edit($id)
    {
        $prodi = Prodi::where('id',$id)->first();
        return view('Prodi.edit', ['prodi' => $prodi]);
    }

    public function update(Request $request)
    {
        $prodi = Prodi::where('id',$request->id)->update([
            'nama' => $request->nama,
        ]);
        return redirect('/prodi');
    }

    public function destroy($id)
    {
        $prodi = Prodi::where('id',$id)->delete();
        return redirect('/prodi');
    }
}
