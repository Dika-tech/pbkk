<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'prodi_id', 'nim', 'nama', 'jenis_kelamin',  'alamat'];

    public function prodi()
    {
        return $this->hasOne('App\Models\Prodi','id','prodi_id');
    }
}
